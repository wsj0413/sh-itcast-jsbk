# 前端技术 #
## ExtJS ##
> **文档：**001-ExtJS-v1.0.doc

- Ext是一个Ajax框架，可以用来开发带有华丽外观的富客户端应用，使得我们的b/s应用更加具有活力及生命力，提高用户体验。
- Ext是一个用javascript编写，与后台技术无关的前端ajax框架。因此，可以把Ext用在.Net、Java、Php等各种开发语言开发的应用中。
- ExtJS的前身来自于YUI，经过不断发展与改进，现在已经成为最完整与成熟的一套构建RIA Web应用的JavaScript基础库。利用ExtJS构建的RIA Web应用具有与桌面程序一样的标准用户界面与操作方式，并且能够横跨不同的浏览器平台。
- ExtJS已经成为开发具有完满用户体验的Web应用完美选择。
- ExtJs最开始基于YUI技术，其UI组件模型和开发理念脱胎、成型于Yahoo组件库YUI和Java平台上Swing两者，并为开发者屏蔽了大量跨浏览器方面的处理。
- 相对来说，EXT要比开发者直接针对DOM、W3C对象模型开发UI组件轻松。

## Node.js ##
> **文档：**002-Node.js-v1.0.doc

- Node.js是一个Javascript运行环境(runtime)。实际上它是对Google V8引擎进行了封装。V8引擎执行Javascript的速度非常快，性能非常好。Node.js对一些特殊用例进行了优化，提供了替代的API，使得V8在非浏览器环境下运行得更好。
- Node.js是一个基于Chrome JavaScript运行时建立的平台， 用于方便地搭建响应速度快、易于扩展的网络应用。Node.js 使用事件驱动， 非阻塞I/O 模型而得以轻量和高效，非常适合在分布式设备上运行的数据密集型的实时应用。
- Node.js使用事件驱动，非阻塞I/O，这样一来，我们的服务器就是轻量级的，并且更加方便，适合做数据密集型的应用和事件应用

## Angularjs ##
> **文档：**003-angularjs-v1.0.doc

AngularJS试图成为WEB应用中的一种端对端的解决方案。这意味着它不只是你的WEB应用中的一个小部分，还是一个完整的端对端的解决方案。这会让AngularJS在构建一个CRUD（增加Create、查询Retrieve、更新Update、删除Delete）的应用时显得很“固执”（原文为 opinionated,意指没有太多的其他方式）。但是，尽管它很“固执”，它仍然能确保它的“固执”只是在你构建应用的起点，并且你仍能灵活变动。AngularJS的一些出众之处如下：
构建一个CRUD应用可能用到的全部内容包括：数据绑定、基本模板标识符、表单验证、路由、深度链接、组件重用、依赖注入。
测试方面包括：单元测试、端对端测试、模拟和自动化测试框架。
具有目录布局和测试脚本的种子应用作为起点。

## jQuery ##
> **文档：**004-jQuery-v1.0.doc

- JQuery是继prototype之后又一个优秀的Javascript库。它是轻量级的js库 ，它兼容CSS3，还兼容各种浏览器（IE 6.0+, FF 1.5+, Safari 2.0+, Opera 9.0+），jQuery2.0及后续版本将不再支持IE6/7/8浏览器。j
- Query使用户能更方便地处理HTML（标准通用标记语言下的一个应用）、events、实现动画效果，并且方便地为网站提供AJAX交互。jQuery还有一个比较大的优势是，它的文档说明很全，而且各种应用也说得很详细，同时还有许多成熟的插件可供选择。
- jQuery能够使用户的html页面保持代码和html内容分离，也就是说，不用再在html里面插入一堆js来调用命令了，只需要定义id即可。
- jQuery是一个兼容多浏览器的javascript库，核心理念是write less,do more(写得更少,做得更多)。jQuery在2006年1月由美国人John Resig在纽约的barcamp发布，吸引了来自世界各地的众多JavaScript高手加入，由Dave Methvin率领团队进行开发。
- 如今，jQuery已经成为最流行的javascript库，在世界前10000个访问最多的网站中，有超过55%在使用jQuery。
- jQuery是免费、开源的，使用MIT许可协议。jQuery的语法设计可以使开发更加便捷，例如操作文档对象、选择DOM元素、制作动画效果、事件处理、使用Ajax以及其他功能。除此以外，jQuery提供API让开发者编写插件。
- 其模块化的使用方式使开发者可以很轻松的开发出功能强大的静态或动态网页。
- jQuery，顾名思义，也就是JavaScript和查询（Query），即是辅助JavaScript开发的库

## jQuery EasyUI ##
> **文档：**005-jQuery EasyUI-v1.0.doc

jQuery EasyUI是一组基于jQuery的UI插件集合体，而jQuery EasyUI的目标就是帮助web开发者更轻松的打造出功能丰富并且美观的UI界面。开发者不需要编写复杂的javascript，也不需要对css样式有深入的了解，开发者需要了解的只有一些简单的html标签。